import React, { Component } from 'react'
import Auth from './Auth';
import fire from './Firebase';
export class EventsList extends Component {
    logout() {
        fire.auth().signOut().then(() => {
          Auth.authenticated = false;
          this.props.history.push('/');
          localStorage.removeItem('userId');
        })

    }
  render() {
    return (
      <div>
        <h1>Events List</h1>
        <p onClick={() => this.props.history.push('updateevent')} >Edit</p>
        <button onClick={() => this.logout()}>Logout</button>
      </div>
    )
  }
}

export default EventsList
